# Dani


## Name
D A N I - Dynamic Artificial Non-Intelligence

## Description
Based in DANI by Sean Davidson
MSX-Computing magazine Feb-Mar 1987
python version: miguel ángel pelegrí
v0.1 * 12/Nov/2021

DANI is a Basic predictive text system
DANI is not ELIZA


## Installation
Program written in Python 3. It does not require any kind of installation.

## Usage
Run the program with:
`python dani.py` or `python3 dani.py`

The first time the program is run, a sqlite database is created that will contain the words that are entered and the number of times it repeats.
If you delete the database, the next time the program is started, an empty database will be created again.
The program prompts for a text entry, and DANI will answer using its knowledge base, and the text patterns used.
You can use the following instructions:
- 'list' or '-l': Displays a table with the knowledge base.
- 'help' or '-h': Displays the help text.
- 'reply' or '-r': Returns a number of replies. If the number of answers is not specified in the text line, it will ask for the number of answers.
- end', 'bye', 'quit', 'exit' or '-q': Exits the program.  

## Roadmap
Possible future developments, a GUI and/or a TUI.

## Contributing
No external contributions are foreseen, but if anyone is interested, I can solicit them.

## Authors and acknowledgment
Programmed by Miguel Ángel Pelegri

## License
CC BY-SA 4.0 DEED
Attribution-ShareAlike 4.0 International

## Project status
Personal project completed
