# D A N I - Dynamic Artificial Non-Intelligence
# Based in DANI by Sean Davidson
# MSX-Computing magazine Feb-Mar 1987
# python version: miguel ángel pelegrí
# v0.1 * 12/Nov/2021

import os
import random
import sqlite3
import logging

intro_text = ("Dynamic Artificial Non-Intelligence\nBased in DANI by Sean Davidson\n"
              "MSX-Computing magazine Feb-Mar 1987\n\nGreetings human!\nThis is your MSX speaking.\n\n")
help_text = ("Talk to me, and I will learn from what you say, and answer you.\nIf you don't think I am learning "
             "anything, type 'list' and I will divulge my knowledge.\nTo leave the conversation, use 'end' or 'bye'."
             "\n\nSpeak now, or forever hold your peace\n")

logging.basicConfig(filename='log.txt', format='%(asctime)s : %(levelname)s : %(message)s', datefmt='%d/%m/%y %H:%M:%S',
                    filemode='a', encoding='UTF-8', level=logging.INFO)


def save_record(from_word, to_word):
    sql = (f'INSERT INTO dani (fromWord, toWord) VALUES ("{from_word}", "{to_word}") '
           f'ON CONFLICT (fromWord, toWord) DO UPDATE SET times = times + 1')
    cur.execute(sql)
    con.commit()


def print_green(green_text):
    print(f"\033[92m {green_text}\033[00m")


def clear_screen():
    match os.name:
        case 'posix':
            os.system("clear")
        case 'ce' | 'nt' | 'dos':
            os.system("cls")


def analyze_text(sentence):
    # Analyze text line, find words
    # 1. Split text into words list
    split_text = sentence.split()
    prev_word = '~'
    for current_word in split_text:
        # 2. Adds the pair "previous word", "current word" in the 'wordLinks' table
        # 3. Update the account if the combination already exists
        if current_word[-1:] == '.':
            current_word = current_word[:-1]
            save_record(prev_word, current_word)
            prev_word = '~'
            save_record(current_word, prev_word)
        else:
            save_record(prev_word, current_word)
            prev_word = current_word


def reply():
    # Shows the system response phrase
    current = '~'
    first_word = True
    reply_text = ''
    while True:
        curr_count = 0
        # 1. Number of times the word has appeared
        sql = f'SELECT SUM (times) FROM dani WHERE fromWord = "{current}"'
        cur.execute(sql)
        row = cur.fetchone()
        times = row[0]  # Counts how many times the searched word has appeared
        # 2. Random word selection
        rnd = random.random() * times  # Random number of words, up to the maximum number of related words
        # Run the query and get all the records with "fromWord"
        cur.execute(f'SELECT fromWord, toWord, times FROM dani WHERE fromWord = "{current}"')
        rows = cur.fetchall()
        # Loop through records until 'curr_count' reaches random number
        for row in rows:
            curr_count += row[2]  # Cumulative amount of database result
            if curr_count > rnd:
                # If it exceeds the marked percentage, it saves the temporary word
                current = row[1]
                if current != '~':
                    # If it is not an end-of-phrase symbol, save the temporary word in the response
                    # and close the for loop
                    if first_word:
                        reply_text += current[0].upper() + current[1:].lower() + ' '
                        first_word = False
                    else:
                        reply_text += current + ' '
                    break
        if current == '~':
            # If the current word is the end-of-phrase symbol, exit the loop
            # reply_loop = False
            break
    # Print the reply in green color
    logging.info(f'c: {reply}')
    return reply_text[:-1] + '.'


def result_table():
    # Print a table with the words saved in the database
    a = '|--------------------|--------------------|----|\n'
    table = a + '| From               | To                 | N  |\n' + a
    cur.execute('SELECT * FROM dani ORDER BY times DESC')
    rows = cur.fetchall()
    for row in rows:
        table += f'|{row[0].ljust(20)}|{row[1].ljust(20)}|{str(row[2]).rjust(4)}|\n'
    table += a
    logging.info(table)
    return table


if __name__ == '__main__':
    # Introduction
    clear_screen()
    print(intro_text)
    logging.info(intro_text)
    print(help_text)
    logging.info(help_text)

    # Database connection
    con = sqlite3.connect('dani.sqlite')
    cur = con.cursor()
    # Try to create table 'vocabulary'
    try:
        con.execute('''CREATE TABLE dani (fromWord text, toWord text, times integer DEFAULT 1, 
        PRIMARY KEY (fromWord, toWord))''')
        logging.info('Created "words" table in database')
    except sqlite3.OperationalError:
        logging.info('Existing table in database')

    sql = 'DELETE FROM dani WHERE fromWord = "~" And toWord = "~"'
    cur.execute(sql)
    con.commit()

    # Main
    while True:
        text_line = input('> ')
        logging.info(f'p: {text_line}')
        match text_line:
            case 'list' | '-l':
                # Prints results table on the screen and in the log file Words that DANI knows
                print(result_table())
                print()

            case 'end' | 'bye' | 'quit' | 'exit' | '-q':
                # End program
                break

            case 'help' | '-h':
                # Print help text
                print(help_text)
                logging.info(help_text)

            case 'reply' | '-r':
                # Print many replies
                n = int(input('How many replies?: '))
                for i in range(n):
                    print_green(reply())
                print()

            case n if text_line[:2] == '-r':
                # Parse reply for more than 1 parameter
                n=int(text_line[2:])
                for i in range(n):
                    print_green(reply())
                print()

            case _:
                # Analyze text line
                analyze_text(f'{text_line} ~')
                print_green(reply())

    # Database connection close
    con.close()
